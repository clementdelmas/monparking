<?php

namespace App\Tests\Common\Entity;

use App\Entity\User;
use App\Tests\Common\Handler\ClassPropertyModifier;
use Faker\Factory;

/**
 * Class FakeUser
 */
class FakeUser
{
    /**
     * Generate a fake user
     *
     * @param string $username
     * @param string $password
     * @param string $firstName
     * @param string $lastName
     * @param string $role
     * @param array $properties
     *
     * @return User
     */
    public static function generate(
        ?string $username = null,
        ?string $password = null,
        ?string $firstName = null,
        ?string $lastName = null,
        string $role = 'ROLE_USER',
        array $properties = []
    ): User {
        $faker = Factory::create();

        $user = new User(
            $username ?? $faker->userName,
            $password ?? $faker->password,
            $firstName ?? $faker->firstName,
            $lastName ?? $faker->lastName,
            $role
        );

        if (!isset($properties['id'])) {
            $properties['id'] = $faker->numberBetween();
        }

        return ClassPropertyModifier::modify($user, $properties);
    }
}
