<?php

namespace App\Tests\Common\Handler;

/**
 * Class ClassPropertyModifier
 */
class ClassPropertyModifier
{
    /**
     * Update a given object with inaccessible properties
     *
     * @param mixed $object
     * @param array $properties
     *
     * @return mixed
     */
    public static function modify($object, array $properties)
    {
        try {
            $userReflection = new \ReflectionClass($object);
            foreach ($properties as $property => $newValue) {
                $property = $userReflection->getProperty($property);
                $property->setAccessible(true);
                $property->setValue($object, $newValue);
                $property->setAccessible(false);
            }
        } catch (\ReflectionException $e) {
        }

        return $object;
    }
}
