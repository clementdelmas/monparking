<?php


namespace App\DTO;


use DateTimeImmutable;


class DTODays
{
    /**
     * @var DateTimeImmutable
     */
    private $date;

    public function getDate(): DateTimeImmutable
    {
        return $this->date;
    }

    public function setDate(DateTimeImmutable $date): void
    {
        $this->date = $date;
    }



    public function __construct()
    {
        $this->date = new DateTimeImmutable();
    }
}
