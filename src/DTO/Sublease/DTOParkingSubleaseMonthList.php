<?php


namespace App\DTO\Sublease;


use App\Entity\ParkingSublease;
use App\Repository\ParkingSubleaseRepository;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;

class DTOParkingSubleaseMonthList
{
    private $takerId;
    private $month;
    private $subleasesToBeCalculated;
    private $subleases;
    private $isPaid;
    private $subleasesTotal;

    public function __construct(int $takerId, string $month, array $subleases)
    {
        $this->takerId = $takerId;
        $this->month = $month;
        $this->subleasesToBeCalculated = $subleases;
        $this->isPaid = true;
        $this->subleasesTotal = 0.0;
    }


    /**
     * @return string
     */
    public function getMonth(): string
    {
        return $this->month;
    }


    /**
     * @param string $month
     */
    public function setMonth(string $month): void
    {
        $this->month = $month;
    }


    /**
     * @return array|null
     */
    public function getSubleases(): ?array
    {
        return $this->subleases;
    }

    /**
     * @param array $subleases
     */
    public function setSubleases(array $subleases): void
    {
        $this->subleases = $subleases;
    }

    /**
     * @return bool
     */
    public function isPaid(): bool
    {
        return $this->isPaid;
    }

    /**
     * @param bool $isPaid
     */
    public function setIsPaid(bool $isPaid): void
    {
        $this->isPaid = $isPaid;
    }

    /**
     * @return float
     */
    public function getSubleasesTotal(): float
    {
        return $this->subleasesTotal;
    }


    /**
     * @param float $subleasesTotal
     */
    public function setSubleasesTotal(float $subleasesTotal): void
    {
        $this->subleasesTotal = $subleasesTotal;
    }


    /**
     * @param ParkingSublease $sublease
     */
    public function addSublease(ParkingSublease $sublease)
    {


        if ($sublease->getDay()->getDate()->format('Y-m') === $this->month) {
            $this->subleases[] = $sublease;


            if ($sublease->getIsNotPaid()) {
                $this->isPaid = false;
            }

            $this->subleasesTotal += $sublease->getPrice();
        }
    }

    public function tableFillingUp(): void
    {
        foreach ($this->subleasesToBeCalculated as $sublease){
            $this->addSublease($sublease);
        }
    }



}
