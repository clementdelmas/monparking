<?php


namespace App\ControllerHelpers;


use App\Entity\ParkingSublease;
use Doctrine\DBAL\Types\BooleanType;

/**
 * Class RentalsData
 * @package App\ControllerHelpers
 */
class RentalsData
{

    /**
     * @var Bool
     */
    private $isWithPrice;

    /**
     * @return bool
     */
    public function getIsWithPrice(): bool
    {
        return $this->isWithPrice;
    }

    public function setIsWithPrice(): void
    {
        $this->isWithPrice = true;
    }

    public function setIsNotWithPrice(): void
    {
        $this->isWithPrice = false;
    }


    /**
     * @param string $subleaseMonth
     * @param string $quarter
     * @return bool
     */
    public function isMonthIncludedIntoQuarter(string $subleaseMonth, string $quarter): bool
    {

        if ($quarter === 'first'){
            $isMonthIntoQuarter = $subleaseMonth === '01' || $subleaseMonth === '02' || $subleaseMonth === '03';
        }elseif ($quarter === 'second'){
            $isMonthIntoQuarter = $subleaseMonth === '04' || $subleaseMonth === '05' || $subleaseMonth === '06';
        }elseif ($quarter === 'third'){
            $isMonthIntoQuarter = $subleaseMonth === '07' || $subleaseMonth === '08' || $subleaseMonth === '09';
        }else{
            $isMonthIntoQuarter = $subleaseMonth === '10' || $subleaseMonth === '11' || $subleaseMonth === '12';
        }

        return $isMonthIntoQuarter;
    }


    /**
     * @param bool $isChosenYearChosenQuarterExisting
     * @param $parkingSublease
     * @param string $chosenYearString
     * @param string $quarter
     * @return bool
     */
    public function quarterCheck(bool $isChosenYearChosenQuarterExisting, $parkingSublease, string $chosenYearString, string $quarter): bool
    {
        if ($isChosenYearChosenQuarterExisting === false) {
            $isMonthIntoQuarter = $this->isMonthIncludedIntoQuarter($this->getSubleaseMonthString($parkingSublease), $quarter);
            if ($this->yearsAndMonthsCompare($chosenYearString, $this->getSubleaseYearString($parkingSublease), $isMonthIntoQuarter)) {
                return true;
            }
        }

        return false;
    }


    /**
     * @param ParkingSublease $parkingSublease
     * @return string
     */
    public function getSubleaseMonthString(ParkingSublease $parkingSublease): string
    {
        return $parkingSublease->getDay()->getDate()->format('m');
    }


    /**
     * @param ParkingSublease $parkingSublease
     * @return string
     */
    public function getSubleaseYearString(ParkingSublease $parkingSublease): string
    {
        return $parkingSublease->getDay()->getDate()->format('Y');
    }


    /**
     * @param string $previousYear
     * @param string $subleaseYear
     * @param bool $isMonthIncludedIntoQuarter
     * @return bool
     */
    public function yearsAndMonthsCompare(string $previousYear, string $subleaseYear, bool $isMonthIncludedIntoQuarter)
    {
        if ($previousYear === $subleaseYear && $isMonthIncludedIntoQuarter) {
            return true;
        }

        return false;
    }




}