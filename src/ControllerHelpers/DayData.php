<?php
/**
 * Created by PhpStorm.
 * User: steg
 * Date: 28.08.2018
 * Time: 21:11
 */

namespace App\ControllerHelpers;


use App\Entity\Day;
use App\Entity\ParkingSublease;
use App\Entity\User;
use App\Repository\ParkingSubleaseRepository;
use DateTimeImmutable;


/**
 * Class DayData
 * @package App\ControllerHelpers
 * Class which gives two information, first the day date, second the potentiel relatid
 * created Day
 */
class DayData
{

    /**
     * @var DateTimeImmutable
     */
    private $date;


    /**
     * @return DateTimeImmutable
     */
    public function getDate() : DateTimeImmutable
    {
        return $this->date;
    }


    /**
     * @var Day
     */
    private $day;


    /**
     * @return Day|null
     */
    public function getDay(): ?Day
    {
        if ($this->day !== null) {

            return $this->day;
        }

        return null;
    }


    /**
     * @return int|null
     */
    public function getDayId(): ?int
    {
        if ($this->day !== null) {
            return $this->day->getId();
        }
        return null;
    }



    /**
     * @return ParkingSublease[]|null
     */
    public function getSubleasedList(): ?array
    {
        return null !== $this->day ? [$this->day->getParkingSublease()] : [];
    }


    /**
     * @return int|null
     */
    public function getIsFreeParkingSubleaseByDay(): ?int
    {
        if ($this->day !==null) {
            return $this->day->getIsFreeParkingSubleaseByDayQuantity();
        }
        return false;
    }



    /**
     * @param ParkingSubleaseRepository $parkingSubleaseRepository
     * @param Day|null $day
     * @param User $user
     * @return ParkingSublease|null
     */
    public function getIsSubleaseOpenedByDayAndUser(ParkingSubleaseRepository $parkingSubleaseRepository, ?Day $day, User $user){

        if ($day !== null) {
            return $parkingSubleaseRepository->findIsSubleaseOpenedByDayAndByUser($day->getId(), $user->getId());
        }

        return null;
    }



    public function getIsSubleaseTakenByDayAndByTaker(ParkingSubleaseRepository $parkingSubleaseRepository, ?Day $day, User $user){

        if ($day !== null) {
            return $parkingSubleaseRepository->findIsSubleaseTakenByDayAndByTaker($day->getId(), $user->getId());
        }

        return null;

    }


    /**
     * @param array $allDays
     * @param DateTimeImmutable $date
     * @return Day|null
     */
    public function getDayFromDate(array $allDays, DateTimeImmutable $date) : ?Day
    {
        foreach($allDays as $day){
            if ($day->getDate()->format('Y-m-d') === $date->format('Y-m-d')){
                return $day;
            }
        }
        return null;
    }


    /**
     * DayData constructor.
     * @param DateTimeImmutable $date
     * @param Day $day
     */
    public function __construct(DateTimeImmutable $date, Day $day)
    {
        $this->date = $date;
        $this->day = $day;

    }
}
