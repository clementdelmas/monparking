<?php


namespace App\Repository;


use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;



/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }


    /**
     * @return User[]
     */
    public function getUsersList(){
        $query = $this->createQueryBuilder('ul')
            ->select('ul')
            ->orderBy('ul.firstName');
        return $query->getQuery()->getResult();

    }


    /**
     * @return User[]
     */
    public function getAllRenterUsersList(){
        $query = $this->createQueryBuilder('ul')
            ->select('ul')
            ->andWhere('ul.role = :role')
            ->setParameter('role', 'ROLE_RENTER')
            ->orderBy('ul.id');
        return $query->getQuery()->getResult();
    }


    /**
     * @param string $role
     * @return User[]
     */
    public function getUsersListPerRole(string $role, User $loggedInUser)
    {
        $query = $this->createQueryBuilder('ul')
            ->select('ul')
            ->andWhere('ul.role = :role')
            ->setParameter('role', $role)
            ->andWhere('ul.isUserEnabled = :enabled')
            ->setParameter('enabled', true)
            ->andWhere('ul.id != :loggedInUserId')
            ->setParameter('loggedInUserId', $loggedInUser->getId())
            ->orderBy('ul.firstName');
        return $query->getQuery()->getResult();
    }


    /**
     * @return User[]
     */
    public function getAllTenantUsersList(){
        $query = $this->createQueryBuilder('ul')
            ->select('ul')
            ->andWhere('ul.role = :role')
            ->setParameter('role', 'ROLE_TENANT')
            ->orderBy('ul.id');
        return $query->getQuery()->getResult();
    }


    /**
     * @param int $parkingNumber
     * @return User[]
     */
    public function findTabUsersByParkingNumber(int $parkingNumber){
        $query = $this->createQueryBuilder('user')
            ->select('user')
            ->andWhere('user.role = :role')
            ->setParameter('role', 'ROLE_RENTER')
            ->andWhere('user.parkingNumber = :parkingNumber')
            ->setParameter('parkingNumber', $parkingNumber);


//        dd($query->getQuery()->getResult());
        return $query->getQuery()->getResult();

    }

}

