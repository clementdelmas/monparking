<?php

namespace App\Repository;

use App\Entity\Day;
use App\Entity\ParkingSublease;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

/**
 * @method Day|null find($id, $lockMode = null, $lockVersion = null)
 * @method Day|null findOneBy(array $criteria, array $orderBy = null)
 * @method Day[]    findAll()
 * @method Day[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DayRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Day::class);
    }


    /**
     * @param ParkingSublease $parkingSublease
     * @return Day|null
     */
    public function findDayByParkingSublease(ParkingSublease $parkingSublease){
        $query = $this->createQueryBuilder('day')
            ->select('day')
            ->innerJoin('day.parkingSublease', 'parkingSublease')
            ->andWhere('parkingSublease.id = :parkingSubleaseId')
            ->setParameter('parkingSubleaseId', $parkingSublease->getId())
            ->andWhere('parkingSublease.isTaken = :isTaken')
            ->setParameter('isTaken', true);

        try {
            return $query->getQuery()->getSingleResult();
        } catch (NoResultException $e) {
        } catch (NonUniqueResultException $e) {
        }

        return null;
    }


//    public function findFirstAvailableParkingSubleaseByDay(Day $day){
//        $query = $this->createQueryBuilder('fa')
//            ->select('fa')
//            ->setParameter('id', $id)
//            ->innerJoin('fa.parkingSublease', 'fapark')
//            ->andWhere('fapark.isSubleaseOpened = :isSubleaseOpened')
//
//        return $query->getQuery()->getResult();
//
//    }


}
