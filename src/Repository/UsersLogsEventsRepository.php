<?php

namespace App\Repository;

use App\Entity\UsersLogsEvents;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method UsersLogsEvents|null find($id, $lockMode = null, $lockVersion = null)
 * @method UsersLogsEvents|null findOneBy(array $criteria, array $orderBy = null)
 * @method UsersLogsEvents[]    findAll()
 * @method UsersLogsEvents[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UsersLogsEventsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UsersLogsEvents::class);
    }

    // /**
    //  * @return UsersLogsEvents[] Returns an array of UsersLogsEvents objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UsersLogsEvents
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
