<?php

namespace App\Repository;

use App\Entity\Messages;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Messages|null find($id, $lockMode = null, $lockVersion = null)
 * @method Messages|null findOneBy(array $criteria, array $orderBy = null)
 * @method Messages[]    findAll()
 * @method Messages[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MessagesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Messages::class);
    }


    /**
     * @param User $loggedInUser
     * @return Messages[] Returns an array of Messages objects
     */
    public function findReceivedAndNotAcknowledgedMessages(User $loggedInUser)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.receiver = :loggedInUser')
            ->setParameter('loggedInUser', $loggedInUser)
            ->andWhere('m.receiptAcknowledgement = :notAcknowledged')
            ->setParameter('notAcknowledged', false)
            ->getQuery()
            ->getResult()
            ;
    }


    /**
     * @param User $loggedInAdministratorUser
     * @param User $user
     * @return Messages[] Returns an array of Messages objects
     */
    public function findReceivedAndNotAcknowledgedByAdministratorAndByUser(User $loggedInAdministratorUser, User $user)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.receiver = :loggedInUser')
            ->setParameter('loggedInUser', $loggedInAdministratorUser)
            ->orWhere('m.receiver = :grUser')
            ->setParameter('grUser', $user)
            ->andWhere('m.receiptAcknowledgement = :false')
            ->setParameter('false', false)
            ->getQuery()
            ->getResult()
            ;
    }


    /**
     * @param User $loggedInAdministratorUser
     * @param User $user
     * @param bool $isArchivedByReceiver
     * @return Messages[] Returns an array of Messages objects
     */
    public function findReceivedByAdministratorAndByUser(User $loggedInAdministratorUser, User $user, bool $isArchivedByReceiver)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.receiver = :loggedInUser')
            ->setParameter('loggedInUser', $loggedInAdministratorUser)
            ->orWhere('m.receiver = :grUser')
            ->setParameter('grUser', $user)
            ->andWhere('m.isArchivedByReceiver = :isArchivedByReceiver')
            ->setParameter('isArchivedByReceiver', $isArchivedByReceiver)
            ->orderBy('m.creationDate', 'DESC')
            ->getQuery()
            ->getResult()
            ;
    }


    /**
     * @param User $loggedInAdministratorUser
     * @param User $user
     * @param bool $isArchivedByTransmitter
     * @return Messages[] Returns an array of Messages objects
     */
    public function findSentByAdministratorAndByUser(User $loggedInAdministratorUser, User $user, bool $isArchivedByTransmitter)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.transmitter = :loggedInUser')
            ->setParameter('loggedInUser', $loggedInAdministratorUser)
            ->orWhere('m.transmitter = :grUser')
            ->setParameter('grUser', $user)
            ->andWhere('m.isArchivedByTransmitter = :isArchivedByTransmitter')
            ->setParameter('isArchivedByTransmitter', $isArchivedByTransmitter)
            ->orderBy('m.creationDate', 'DESC')
            ->getQuery()
            ->getResult()
            ;
    }


    /**
     * @param string $username
     * @return Messages[] Returns an array of Messages objects
     */
    public function findReceivedByUsername(string $username)
    {
        return $this->createQueryBuilder('m')
            ->innerJoin('m.receiver', 'receiver')
            ->andWhere('receiver.username = :username')
            ->setParameter('username', $username)
            ->orderBy('m.creationDate', 'DESC')
            ->getQuery()
            ->getResult()
            ;
    }


    /*
    public function findOneBySomeField($value): ?Messages
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
