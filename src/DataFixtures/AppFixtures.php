<?php

namespace App\DataFixtures;

use App\Entity\ParkingSublease;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        $this->loadDev($manager);
        $manager->flush();
    }

    private function loadDev(ObjectManager $manager)
    {

//        ************************
//        Renters
//        ************************
        $password = 'passpass';
//        $renterA = new User('preteurA', $password, 'Prêteur','A', 'ROLE_RENTER');
//        $renterA->setParkingNumber(41);
//        $manager->persist($renterA);
//
//        $renterB = new User('preteurB', $password, 'Prêteur','B', 'ROLE_RENTER');
//        $renterB->setParkingNumber(27);
//        $manager->persist($renterB);


//        ************************
//        Tenants
//        ************************
//        $tenantA = new User('emprunteurA', $password, 'Emprunteur','A', 'ROLE_TENANT');
//        $manager->persist($tenantA);
//
//        $tenantB = new User('emprunteurB', $password, 'Emprunteur','B', 'ROLE_TENANT');
//        $manager->persist($tenantB);

        // parkings

        // A loue sa place à A



//        ************************
//        Administrator
//        ************************
        $administrator = new User('admin', $password, 'Administrateur','Admin', 'ROLE_ADMINISTRATOR');
        $manager->persist($administrator);

        $manager->flush();

    }

}
