<?php


namespace App\Controller;


use App\Entity\Day;
use App\Entity\ParkingSublease;
use App\Entity\User;
use App\Repository\DayRepository;
use App\Repository\ParkingSubleaseRepository;
use DateTimeImmutable;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use LogicException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;


class DayFreeUp extends AbstractController
{

    /**
     * @param string $dateString
     * @param EntityManagerInterface $entityManager
     * @param DayRepository $dayRepository
     * @param ParkingSubleaseRepository $parkingSubleaseRepository
     * @param AuthorizationCheckerInterface $authChecker
     * @return Response
     * @throws Exception
     * @Security("is_granted('ROLE_RENTER')")
     * @Route("/dayfreeup/{dateString}", name="day_free_up", requirements={"dateString" = "\d{4}\-\d{2}\-\d{2}"})
     */
    public function __invoke(string $dateString, EntityManagerInterface $entityManager, DayRepository $dayRepository, ParkingSubleaseRepository $parkingSubleaseRepository, AuthorizationCheckerInterface $authChecker) : Response
    {

        $securityUser = $this->getUser();

        /**
         * @var User $loggedInUser
         */
        $loggedInUser = $securityUser->getUser();

        $isLoggedInUserEnabled = $loggedInUser->getIsUserEnabled();


        $today = new DateTimeImmutable('midnight');

        /**
         * Gandi server displays two hours less than Swiss time
         */
//        $today = $today->modify('-2 hours');

        $todayPlusOneMonth = $today->modify('+1 month');
        $dateToBeFreedUp = new DateTimeImmutable($dateString);

        $isDateWithinOneMonth = $dateToBeFreedUp <= $todayPlusOneMonth;

        $isDatePriorToTodaysDate = $dateToBeFreedUp < $today;

        if (!$isDateWithinOneMonth or $isDatePriorToTodaysDate){
            throw new LogicException('Chosen date is not within today plus one month or is prior to today');
        }

        $securityUser = $this->getUser();

        /* @var User $user */
        $user = $securityUser->getUser();

        $newDay = $dayRepository->findOneBy(['date' => $dateToBeFreedUp]);


        /**
         * if Day instance related to the chosen date does not exist it has to be created
         */
        if (!$newDay) {

            if (!$isLoggedInUserEnabled){
                throw $this->createAccessDeniedException('Logged in user has been disabled');
            }

            $newDay = new Day($dateToBeFreedUp);

            $this->getDoctrine()->getManager()->persist($newDay);
            $entityManager->flush();

            $newParkingSublease = new ParkingSublease($newDay, $user);
            $newDay->isFreeParkingSubleaseByDayIncreaseByOne();

            $this->getDoctrine()->getManager()->persist($newParkingSublease);
            $entityManager->flush();

//            $newDay->getParkingSublease()->add($newParkingSublease);

            $this->getDoctrine()->getManager()->persist($newParkingSublease);
            $entityManager->flush();



            /**
             * if Day instance related to the chosen date already exists, we look for all of the related ParkingSubleases related to that
             * day and analyse them and check if the ParkingSublease is opened or taken and if the connected user is related to
             * those ParkingSubleases
             */
        } else {

            $subleaseRelatedToTheDayAndToLoggedUser = $parkingSubleaseRepository->findOpenedSubleaseByDayAndByUser($newDay->getId(), $user->getId());

            if ($subleaseRelatedToTheDayAndToLoggedUser !== null) {

                if ($subleaseRelatedToTheDayAndToLoggedUser->getIsSubleaseOpened()) {
                    $subleaseRelatedToTheDayAndToLoggedUser->unSublease();
                    $entityManager->remove($subleaseRelatedToTheDayAndToLoggedUser);
                    $newDay->isFreeParkingSubleaseByDayDecreaseByOne();

                }
            }
            else{
                if (!$isLoggedInUserEnabled){
                    throw $this->createAccessDeniedException('Logged in user has been disabled');
                }
                $newParkingSublease = new ParkingSublease($newDay, $user);
                $newDay->isFreeParkingSubleaseByDayIncreaseByOne();
                $this->getDoctrine()->getManager()->persist($newParkingSublease);
            }
        }

//        $this->getDoctrine()->getManager()->persist($newDay);
        $entityManager->flush();

//        $test = $parkingSubleaseRepository->findSubleaseByDay($newDay->getId());
//        dd($test);

        return $this->redirectToRoute(
            'calendar', [
            'year' => $dateToBeFreedUp->format('Y'),
            'month' => $dateToBeFreedUp->format('m'),
        ]);
    }
}
