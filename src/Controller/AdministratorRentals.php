<?php


namespace App\Controller;


use App\Repository\ParkingSubleaseRepository;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdministratorRentals extends AbstractController
{

    /**
     * @param ParkingSubleaseRepository $parkingSubleaseRepository
     * @param Request $request
     * @return Response
     * @throws Exception
     * @Security("is_granted('ROLE_ADMINISTRATOR')")
     * @Route("/myAdministratorRentals/", name="my_administrator_rentals")
     */
    public function __invoke(ParkingSubleaseRepository $parkingSubleaseRepository, Request $request): Response
    {
        // Check if user's referer is 'calendar' (needed to display 'back' button)
        $referer = $request->headers->get('referer', '/');
        $isRefererCalendar = strpos($referer, 'calendar') !== false;

        $subleasesList = $parkingSubleaseRepository->findTabIsSubleaseTakenOrOpened();

        return $this->render('AdministratorRentals.twig', [
            'subleases_list' => $subleasesList,
            'referer' => $referer,
            'is_referer_calendar' => $isRefererCalendar,
        ]);
    }
}
