<?php


namespace App\Controller;


use App\ControllerHelpers\Security\SecurityUser;
use App\Entity\User;
use App\Entity\UsersLogsEvents;
use App\Repository\ParkingSubleaseRepository;
use DateTimeImmutable;
use DateTimeZone;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RenterRentals extends AbstractController
{

    /**
     * @param ParkingSubleaseRepository $parkingSubleaseRepository
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return Response
     * @throws Exception
     * @Security("is_granted('ROLE_RENTER')")
     * @Route("/myRenterRentals/", name="my_renter_rentals")
     */
    public function __invoke(ParkingSubleaseRepository $parkingSubleaseRepository, Request $request,
                             EntityManagerInterface $entityManager): Response
    {

        /* @var SecurityUser $securityUser */
        $securityUser = $this->getUser();

        $loggedInUser = $securityUser->getUser();

        if ((!$this->isGranted('ROLE_ADMINISTRATOR')) && (!$this->isGranted('ROLE_PREVIOUS_ADMIN'))) {
            $loginEvent = new UsersLogsEvents($loggedInUser,
                new DateTimeImmutable('', new DateTimeZone('Europe/Paris')), 'Prêts/emprunts actuels');
            $entityManager->persist($loginEvent);
            $entityManager->flush();
        }


        $referer = $request->headers->get('referer', '/');

        $isRefererCalendar = strpos($referer, 'calendar') !== false;

        /**
         * Period within one month
         */
        $today = new DateTimeImmutable('midnight');

        /**
         * Gandi server displays two hours less than Swiss time
         */

        $todayPlusOneMonth = $today->modify('+1 month');

        $currentPeriodSubleasesList = $parkingSubleaseRepository->findTabIsSubleaseTakenByRenterBetweenTwoDates($loggedInUser->getId(), $today, $todayPlusOneMonth);


        /**
         * Begin period determination
         */
        $currentYear = new DateTimeImmutable('midnight');
        $currentYearString = $currentYear->format('Y');

        $previousYear = $currentYear->modify('-1 year');
        $previousYearString = $previousYear->format('Y');

        $beginPeriod = $currentYear->modify('first day of January this year');

        $subleasesList = $parkingSubleaseRepository->findTabIsSubleaseTakenByRenter($loggedInUser->getId(), $beginPeriod);

        return $this->render('RenterRentals.twig', [
            'SubleasesList' => $subleasesList,
            'CurrentPeriodSubleasesList' => $currentPeriodSubleasesList,
            'User' => $loggedInUser,
            'Referer' => $referer,
            'CurrentYearString' => $currentYearString,
            'PreviousYearString' => $previousYearString,
            'IsRefererCalendar' => $isRefererCalendar,

        ]);

    }
}
