<?php

namespace App\Controller;

use App\ControllerHelpers\Security\SecurityUser;
use App\Entity\UsersLogsEvents;
use App\Repository\MessagesRepository;
use App\Repository\UserRepository;
use DateTimeImmutable;
use DateTimeZone;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MessageReceiveController extends AbstractController
{
    /**
     * @Route("/message/receive/{inboxType}", name="message_receive", requirements={"inboxType" = "[a-z-A-Z]+"})
     * @Security("is_granted('ROLE_ADMINISTRATOR') or is_granted('ROLE_RENTER') or is_granted('ROLE_TENANT')")
     * @param MessagesRepository $messagesRepository
     * @param UserRepository $userRepository
     * @param string $inboxType
     * @param EntityManagerInterface $entityManager
     * @return Response
     * @throws Exception
     */
    public function __invoke(MessagesRepository $messagesRepository, UserRepository $userRepository, string $inboxType,
                             EntityManagerInterface $entityManager): Response
    {
        /* @var SecurityUser $securityUser */
        $securityUser = $this->getUser();
        $loggedInReceiver = $securityUser->getUser();

        if ((!$this->isGranted('ROLE_ADMINISTRATOR')) && (!$this->isGranted('ROLE_PREVIOUS_ADMIN'))) {
            $loginEvent = new UsersLogsEvents($loggedInReceiver,
                new DateTimeImmutable('', new DateTimeZone('Europe/Paris')), 'Liste messages reçus');
            $entityManager->persist($loginEvent);
            $entityManager->flush();
        }


        $receivedMessagesList = [];

        $grReceiverUser = $userRepository->findOneBy(['username' => 'gr']);


        if ($this->isGranted('ROLE_ADMINISTRATOR') && $inboxType === 'inbox') {
            $receivedMessagesList = $messagesRepository->findReceivedByAdministratorAndByUser($loggedInReceiver, $grReceiverUser, false);
        }elseif ($this->isGranted('ROLE_ADMINISTRATOR') && $inboxType === 'inbox') {
            $receivedMessagesList = $messagesRepository->findSentByAdministratorAndByUser($loggedInReceiver, $grReceiverUser, false);
        }elseif ($this->isGranted('ROLE_PREVIOUS_ADMIN') && $inboxType === 'archived') {
            $receivedMessagesList = $messagesRepository->findBy(['receiver' => $loggedInReceiver,
                'isArchivedByReceiver' => true], ['creationDate' => 'DESC']);
        }elseif ($this->isGranted('ROLE_PREVIOUS_ADMIN') && $inboxType === 'sentArchived') {
            $receivedMessagesList = $messagesRepository->findBy(['transmitter' => $loggedInReceiver,
                'isArchivedByTransmitter' => true], ['creationDate' => 'DESC']);
        }elseif ($inboxType === 'inbox') {
            $receivedMessagesList = $messagesRepository->findBy(['receiver' => $loggedInReceiver,
                'isArchivedByReceiver' => false], ['creationDate' => 'DESC']);
        } elseif ($inboxType === 'archived' && $this->isGranted('ROLE_ADMINISTRATOR')) {
            $receivedMessagesList = $messagesRepository->findReceivedByAdministratorAndByUser($loggedInReceiver, $grReceiverUser, true);
        } elseif ($inboxType === 'sentArchived' && $this->isGranted('ROLE_ADMINISTRATOR')) {
            $receivedMessagesList = $messagesRepository->findSentByAdministratorAndByUser($loggedInReceiver, $grReceiverUser, true);
        }


        return $this->render('message_receive/index.html.twig', [
            'ReceivedMessagesList' => $receivedMessagesList,
            'InboxType' => $inboxType,
        ]);
    }
}
