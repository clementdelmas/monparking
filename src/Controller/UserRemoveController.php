<?php

namespace App\Controller;

use App\Entity\User;
use App\Repository\ParkingSubleaseRepository;
use Doctrine\ORM\EntityManagerInterface;
use LogicException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserRemoveController extends AbstractController
{
    /**
     * @Route("/user/remove/{userId}", name="user_remove", requirements={"userId" = "\d+"})
     * @Entity("user", expr="repository.find(userId)")
     * @param ParkingSubleaseRepository $parkingSubleaseRepository
     * @param Request $request
     * @param User $user
     * @param FormFactoryInterface $formFactory
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function __invoke(ParkingSubleaseRepository $parkingSubleaseRepository, Request $request, User $user, FormFactoryInterface $formFactory, EntityManagerInterface $entityManager): Response
    {

        $subleasesList = $parkingSubleaseRepository->findTabSubleasesByUser($user);

        if (!empty($subleasesList)){
            throw new LogicException('That user is related to at least one parkingSublease. That user can\'t be removed.');
        }

        $entityManager->remove($user);

        $entityManager->flush();

        return $this->redirectToRoute('users_list');
    }
}
