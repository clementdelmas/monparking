<?php

namespace App\Controller;


use App\DTO\DTOUserRenterUpdate;
use App\DTO\DTOUserTenantUpdate;
use App\Entity\User;
use App\Form\RenterUserUpdateType;
use App\Form\TenantUserUpdateType;
use App\Repository\UserRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

class UserUpdate extends AbstractController
{
    /**
     * @Route("/user/update/{userId}", name="user_update")
     * @Entity("user", expr="repository.find(userId)")
     * @param Request $request
     * @param FormFactoryInterface $formFactory
     * @param UserRepository $userRepository
     * @param User $user
     * @return Response
     * @Security("is_granted('ROLE_ADMINISTRATOR')")
     */
    public function __invoke(Request $request, FormFactoryInterface $formFactory, UserRepository $userRepository, User $user): Response
    {

        $referer = $request->headers->get('referer', '/');

//        $chosenUser = $userRepository->find($user->getId());

        $chosenUser = $user;

        if ($chosenUser->getRole() === 'ROLE_RENTER') {
            $DTOUser = new DTOUserRenterUpdate();
        }else{
            $DTOUser = new DTOUserTenantUpdate();
        }
        $DTOUser->setUsername($chosenUser->getUsername());
        $DTOUser->setLastName($chosenUser->getLastName());
        $DTOUser->setFirstName($chosenUser->getFirstName());
        if ($chosenUser->getRole() === 'ROLE_RENTER'){
            $DTOUser->setParkingNumber($chosenUser->getParkingNumber());
        }

        if ($chosenUser->getRole() === 'ROLE_RENTER'){
            $form = $formFactory->create(RenterUserUpdateType::class, $DTOUser);
            $form->handleRequest($request);
        }else{
            $form = $formFactory->create(TenantUserUpdateType::class, $DTOUser);
            $form->handleRequest($request);
        }


        if ($form->isSubmitted()) {

            $data = $form->getData();

            $newUsername = $data->getUsername();

            $existingUser = $userRepository->findOneBy(['username' => $newUsername]);

            if ($existingUser !== null && $chosenUser->getUsername() !== $newUsername) {
                $form->addError(new FormError('Nom d\'utilisateur déjà utilisé'));
            }

            if ($chosenUser->getRole() === 'ROLE_RENTER') {
                $newParkingNumber = $data->getParkingNumber();
                $existingParkingNumberTab = $userRepository->findTabUsersByParkingNumber($newParkingNumber);

                if (!(empty($existingParkingNumberTab)) && $chosenUser->getParkingNumber() !== $newParkingNumber) {
                    $form->addError(new FormError('No de parking appartenant déjà à un autre prêteur'));
                }
            }


            if ($form->isValid()) {

//                $chosenUser = $userRepository->find($userId);

                $newUsername = $data->getUsername();
                $newLastName = $data->getLastName();
                $newFirstName = $data->getFirstName();

                if ($chosenUser->getRole() === 'ROLE_RENTER') {
                    $newParkingNumber = $data->getParkingNumber();
                }else{
                    $newParkingNumber = null;
                }

                $chosenUser->changeUsername($newUsername);

                $chosenUser->changeLastName($newLastName);
                $chosenUser->changeFirstName($newFirstName);

                if ($chosenUser->getRole() === 'ROLE_RENTER') {
                    $chosenUser->changeParkingNumber($newParkingNumber);
                }

//                $this->getDoctrine()->getManager()->persist($chosenUser);
                $this->getDoctrine()->getManager()->flush();
                return $this->redirectToRoute('user_consulting', ['userId' => $chosenUser->getId()]);
            }
        }



        if ($chosenUser->getRole() === 'ROLE_RENTER') {
            return $this->render('UserRenterUpdate.twig', [
                'formular' => $form->createView(),
                'ExistingUser' => $chosenUser,
                'Referer' => $referer,
            ]);
        }else{
            return $this->render('UserTenantUpdate.twig', [
                'formular' => $form->createView(),
                'ExistingUser' => $chosenUser,
                'Referer' => $referer,
            ]);
        }
    }
}
