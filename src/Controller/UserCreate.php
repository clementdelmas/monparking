<?php

namespace App\Controller;


use App\DTO\DTOUserRenter;
use App\DTO\DTOUserTenant;
use App\Entity\User;
use App\Form\RenterUserType;
use App\Form\TenantUserType;
use App\Repository\UserRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class UserCreate extends AbstractController
{
    /**
     * @Route("/user/create/{role}", name="user_create")
     * @param Request $request
     * @param FormFactoryInterface $formFactory
     * @param UserRepository $userRepository
     * @param $role
     * @return Response
     * @Security("is_granted('ROLE_ADMINISTRATOR')")
     */
    public function __invoke(Request $request, FormFactoryInterface $formFactory, UserRepository $userRepository,
                             string $role): Response
    {
        if ($role === 'ROLE_TENANT') {
            $form = $formFactory->create(TenantUserType::class, new DTOUserTenant());
        }elseif ($role === 'ROLE_RENTER'){
            $form = $formFactory->create(RenterUserType::class, new DTOUserRenter());
        }

        $form->handleRequest($request);

        if ($form->isSubmitted()) {


            /**
             * @var User $data
             */
            $data = $form->getData();

            $existingUser = $userRepository->findOneBy(['username' => $data->getUsername()]);
            if($existingUser instanceof User){
                $form->addError(new FormError('Nom d\'utilisateur déjà utilisé'));
            }

            if ($role === 'ROLE_RENTER') {
                $parkingNumber = $data->getParkingNumber();
                $existingParkingNumberTab = $userRepository->findTabUsersByParkingNumber($parkingNumber);
                if (!(empty($existingParkingNumberTab))) {
                    $form->addError(new FormError('No de parking appartenant déjà à un autre prêteur'));
                }
            }

            if ($form->isValid()) {

                $username = $data->getUsername();
                $password = $data->getPassword();
                $lastName = $data->getLastName();
                $firstName = $data->getFirstName();

                if ($role === 'ROLE_RENTER') {
                    $parkingNumber = $data->getParkingNumber();
                }else{
                    $parkingNumber = null;
                }

                $user = new User($username, $password, $lastName, $firstName, $role);

                if ($role === 'ROLE_RENTER') {
                    $user->setParkingNumber($parkingNumber);
                }

                $this->getDoctrine()->getManager()->persist($user);
                $this->getDoctrine()->getManager()->flush();
                return $this->redirectToRoute('users_list');
            }
        }

        return $this->render('UserCreate.twig', [
            'formular' => $form->createView(),
            'Role' => $role,
        ]);

    }
}
