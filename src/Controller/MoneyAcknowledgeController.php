<?php

namespace App\Controller;

use App\Entity\ParkingSublease;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\User;
use App\Repository\ParkingSubleaseRepository;
use App\Repository\UserRepository;
use DateTimeImmutable;
use Exception;
use LogicException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MoneyAcknowledgeController extends AbstractController
{
    /**
     * @Route("/money/acknowledge/{takerId}/{month}/{category}", name="money_acknowledge", requirements={"takerId" = "\d+", "month" = "\d{4}\-\d{2}", "category" = "[a-z-A-Z]+"})
     * @Security("is_granted('ROLE_RENTER') or is_granted('ROLE_PREVIOUS_ADMIN')")
     * @param string $takerId
     * @param string $month
     * @param UserRepository $userRepository
     * @param string $category
     * @param Request $request
     * @param ParkingSubleaseRepository $parkingSubleaseRepository
     * @param EntityManagerInterface $entityManager
     * @return Response
     * @throws Exception
     */
    public function __invoke(string $takerId, string $month, UserRepository $userRepository, string $category,
                             Request $request, ParkingSubleaseRepository $parkingSubleaseRepository,
                             EntityManagerInterface $entityManager): Response
    {


        /* @var User $loggedRenterUser */
        $loggedRenterUser = $this->getUser()->getUser();
        $userId = $loggedRenterUser->getId();

        $beginDate = new DateTimeImmutable($month);
        $endDate = $beginDate->modify('last day of this month');


        $userSubleases = [];

        if ($loggedRenterUser->getRole() === 'ROLE_RENTER') {
            $userSubleases = $parkingSubleaseRepository->findTabIsSubleaseTakenByRenterAndByTakerBetweenTwoDates($userId,
                $takerId, $beginDate, $endDate);
        }elseif ($loggedRenterUser->getRole() === 'ROLE_TENANT'){
            $userSubleases = $parkingSubleaseRepository->findTabIsSubleaseTakenByRenterAndByTakerBetweenTwoDates($takerId,
                $userId, $beginDate, $endDate);
        }

        /* @var ParkingSublease $anySublease */
        $anySublease = reset($userSubleases);
        $anySubleaseRenterUser = $anySublease->getUser();


        if ($anySubleaseRenterUser !== $loggedRenterUser && !$this->isGranted('ROLE_PREVIOUS_ADMIN')){
            throw new LogicException('Logged in user not allowed');
        }

        if ($anySubleaseRenterUser->getUsername() === 'gr'){
            throw new LogicException('Logged in used doesn\'t get money');
        }

        foreach ($userSubleases as $sublease){
            $sublease->markAsPaid();
        }

        $entityManager->flush();


        return $this->redirectToRoute('accounting_month_edit', ['month' => $month, 'contractorId' => $takerId, 'userId' => $userId, 'category' => $category]);

//        path('accounting_month_edit',
//                                               {month:monthItems.month, contractorId:monthItems.contractor.id,
//                                                   userId:LoggedUser.id}) }}">
//
//        if ($category === 'isNotPaid') {
//            return $this->redirectToRoute('accounting', ['category' => 'isNotPaid']);
//        }else{
//            return $this->redirectToRoute('accounting', ['category' => 'all']);
//        }

    }
}
