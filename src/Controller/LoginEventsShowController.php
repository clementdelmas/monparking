<?php

namespace App\Controller;

use App\Repository\UsersLogsEventsRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LoginEventsShowController extends AbstractController
{
    /**
     * @Route("/login/events/show", name="login_events_show")
     * @Security("is_granted('ROLE_ADMINISTRATOR')")
     * @param UsersLogsEventsRepository $usersLogsEventsRepository
     * @return Response
     */
    public function __invoke(UsersLogsEventsRepository $usersLogsEventsRepository): Response
    {
        $usersLogs = $usersLogsEventsRepository->findBy([],['loginDate' => 'DESC']);

        return $this->render('login_events_show/index.html.twig', [
            'usersLogs' => $usersLogs,
        ]);
    }
}
