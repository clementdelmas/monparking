<?php

namespace App\Controller;

use App\ControllerHelpers\CalendarData;
use App\DTO\Sublease\DTOParkingSubleaseTakerList;
use App\Entity\User;
use App\Repository\ParkingSubleaseRepository;
use App\Repository\UserRepository;
use DateTimeImmutable;
use Exception;
use LogicException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AccountingMonthEditController extends AbstractController
{
    /**
     * @Route("/accounting/month/edit/{month}/{contractorId}/{userId}/{category}", name="accounting_month_edit",
     *     requirements={"month" = "\d{4}-\d{1,2}", "contractorId" = "\d+", "userId" = "\d+", "category" = "[a-z-A-Z]+"})
     * @Security("is_granted('ROLE_PREVIOUS_ADMIN') or is_granted('ROLE_RENTER') or is_granted('ROLE_TENANT')")
     * @Entity("user", expr="repository.find(userId)")
     * @Entity("contractor", expr="repository.find(contractorId)")
     * @param $month
     * @param User $contractor
     * @param User $user
     * @param string $category
     * @param ParkingSubleaseRepository $parkingSubleaseRepository
     * @param UserRepository $userRepository
     * @param Request $request
     * @return Response
     * @throws Exception
     */
    public function __invoke($month, User $contractor, User $user, string $category, ParkingSubleaseRepository $parkingSubleaseRepository,
                             UserRepository $userRepository, Request $request): Response
    {

        $beginDate = new DateTimeImmutable($month);
        $endDate = $beginDate->modify('last day of this month');

        $userSubleases = [];

        $url = $this->generateUrl('accounting', ['category' => $category]);


        $referer = $request->headers->get('referer', '/');

        if ($user->getRole() === 'ROLE_RENTER') {
            $userSubleases = $parkingSubleaseRepository->findTabIsSubleaseTakenByRenterAndByTakerBetweenTwoDates($user->getId(),
                $contractor->getId(), $beginDate, $endDate);
        }elseif ($user->getRole() === 'ROLE_TENANT') {
            $userSubleases = $parkingSubleaseRepository->findTabIsSubleaseTakenByRenterAndByTakerBetweenTwoDates($contractor->getId(),
                $user->getId(), $beginDate, $endDate);
        }

        $calendarData = new CalendarData([]);
        $monthString = $calendarData->toStringFromMonthAndYearString($month);

        if ($this->isGranted('ROLE_RENTER')){
            $key = (array_key_first($userSubleases));
            $loggedInRenterUser = $this->getUser()->getUser();
            if (($userSubleases[$key])->getUser() !== $loggedInRenterUser){
                throw new LogicException('Logged-in renter user not allowed to do this operation');
            }
        }

        $isPaidSum = 0;
        $isNotPaidSum = 0;
        $totalDueAmount = 0;

        foreach ($userSubleases as $sublease){
            if ($sublease->getIsPaid()){
                $isPaidSum += $sublease->getPrice();
            }elseif($sublease->getIsNotPaid()){
                $isNotPaidSum += $sublease->getPrice();
            }
            $totalDueAmount += $sublease->getPrice();
        }


        $mustRefererAppear = (strpos($referer, 'isPaid') || strpos($referer, 'isNotPaid') || strpos($referer, 'all') !== false);

        return $this->render('accounting_month_edit/index.html.twig', [
            'UserSubleases' => $userSubleases,
            'Referer' => $referer,
            'url' => $url,
            'category' => $category,
            'MustRefererAppear' => $mustRefererAppear,
            'isPaidSum' => $isPaidSum,
            'isNotPaidSum' => $isNotPaidSum,
            'Month' => $month,
            'MonthString' => $monthString,
            'User' => $user,
            'Contractor' => $contractor,
            'TotalDueAmount' => $totalDueAmount,
        ]);
    }
}
