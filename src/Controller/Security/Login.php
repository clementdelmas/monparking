<?php


namespace App\Controller\Security;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;


class Login extends AbstractController
{

    /**
     * @Route("/login", name="login")
     * @param AuthenticationUtils $authentificationUtils
     * @param Request $request
     * @return Response
     */
    public function __invoke(AuthenticationUtils $authentificationUtils, Request $request)
    {

        $error = $authentificationUtils->getLastAuthenticationError();

        $lastUsername = $authentificationUtils->getLastUsername();

        $referer = $request->headers->get('referer', '/');
        $isRefererLogin = (strpos($referer, 'login')) !== false;

        $session = new Session();

        if(!$session->has('previous')) {
            $session->set('previous', $referer);
        }

        $ses = $session->get('previous');

        return $this->render('Login.twig', [
            'last_username' => $lastUsername,
            'error' => $error,
            'referer' => $referer,
            'isRefererLogin' => $isRefererLogin,
            'session' => $ses,
        ]);
    }
}
