<?php


namespace App\Controller\Security;


use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;

//class LogoutDirection implements LogoutSuccessHandlerInterface
class LogoutDirection implements LogoutSuccessHandlerInterface

{

    private $urlGenerator;

    /**
     * Creates a Response object to send upon a successful logout.
     *
     * @Route("/logoutdirection", name="logoutdirection")
     * @param Request $request
     * @return Response never null
     */
    public function onLogoutSuccess(Request $request)
    {

        $referer = $request->headers->get('referer', '/');

        $calendarResearch = 'calendar';
        $calendarResearchResult = strpos($referer, $calendarResearch);

        if ($calendarResearchResult !== false){
            return new RedirectResponse($referer);
        }

        $default = $this->urlGenerator->generate('calendar_redirect');

        return new RedirectResponse($default);

    }

    public function __construct(UrlGeneratorInterface $urlGenerator)
    {
        $this->urlGenerator = $urlGenerator;

    }

}
