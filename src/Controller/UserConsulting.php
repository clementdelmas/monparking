<?php


namespace App\Controller;


use App\ControllerHelpers\Security\SecurityUser;
use App\DTO\DTOChangePassword;
use App\Entity\User;
use App\Entity\UsersLogsEvents;
use App\Form\ChangePasswordType;
use App\Repository\ParkingSubleaseRepository;
use App\Repository\UserRepository;
use DateTimeImmutable;
use DateTimeZone;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class UserConsulting extends AbstractController
{


    /**
     * @Security("is_granted('ROLE_ADMINISTRATOR') or is_granted('ROLE_RENTER') or is_granted('ROLE_TENANT')")
     * @Route("/userConsulting/{userId}", name="user_consulting", requirements={"userId" = "\d+"})
     * @Entity("user", expr="repository.find(userId)")
     * @param UserRepository $userRepository
     * @param Request $request
     * @param User $user
     * @param FormFactoryInterface $formFactory
     * @param ParkingSubleaseRepository $parkingSubleaseRepository
     * @param EntityManagerInterface $entityManager
     * @return Response
     * @throws Exception
     */
    public function __invoke(UserRepository $userRepository, Request $request, User $user,
                             FormFactoryInterface $formFactory,
                             ParkingSubleaseRepository $parkingSubleaseRepository,
                             EntityManagerInterface $entityManager): Response
    {

        /**
         * @var SecurityUser $securityUser
         */
        $securityUser = $this->getUser();
        $loggedInUser = $securityUser->getUser();

        if ((!$this->isGranted('ROLE_ADMINISTRATOR')) && (!$this->isGranted('ROLE_PREVIOUS_ADMIN'))) {
            $loginEvent = new UsersLogsEvents($loggedInUser,
                new DateTimeImmutable('', new DateTimeZone('Europe/Paris')), 'Mon profil');
            $entityManager->persist($loginEvent);
            $entityManager->flush();
        }


        $currentYear = new DateTimeImmutable('midnight');
        $currentYearString = $currentYear->format('Y');

        $previousYear = $currentYear->modify('-1 year');
        $previousYearString = $previousYear->format('Y');

//        $consultedUser = $userRepository->find($userId);

        if ((!$this->isGranted('ROLE_ADMINISTRATOR')) && $loggedInUser !== $user){
            throw $this->createAccessDeniedException('Logged in user not allowd to access the consulted user details');
        }

        $form = $formFactory->create(ChangePasswordType::class, new DTOChangePassword());
        $form->handleRequest($request);

        if($form->isSubmitted()) {
            $data = $form->getData();

            if ($form->isValid()) {
                $newPassword = $data->getPassword();
                $user->changePassword($newPassword);
                $this->getDoctrine()->getManager()->flush();
                $this->addFlash("success",'Le mot de passe a bien été modifié');
                return $this->redirectToRoute('user_consulting', ['userId' => $user->getId()]);
            }
        }

        $subleasesList = $parkingSubleaseRepository->findTabSubleasesByUser($user);


        if (!empty($subleasesList)){
            $relatedSubleasesCounter = count($subleasesList);
        }else{
            $relatedSubleasesCounter = 0;
        }

        return $this->render('UserConsulting.twig', [
            'CurrentYearString' => $currentYearString,
            'PreviousYearString' => $previousYearString,
            'ConsultedUser' => $user,
            'LoggedInUser' => $loggedInUser,
            'formularPass' => $form->createView(),
            'RelatedSubleasesCounter' => $relatedSubleasesCounter,
        ]);
    }
}
