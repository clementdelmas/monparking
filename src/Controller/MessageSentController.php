<?php

namespace App\Controller;

use App\ControllerHelpers\Security\SecurityUser;
use App\Entity\UsersLogsEvents;
use App\Repository\MessagesRepository;
use DateTimeImmutable;
use DateTimeZone;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MessageSentController extends AbstractController
{
    /**
     *
     * @Route("/message/sent", name="message_sent")
     * @Security("is_granted('ROLE_ADMINISTRATOR') or is_granted('ROLE_RENTER') or is_granted('ROLE_TENANT')")
     * @param MessagesRepository $messagesRepository
     * @param EntityManagerInterface $entityManager
     * @return Response
     * @throws Exception
     */
    public function __invoke(MessagesRepository $messagesRepository, EntityManagerInterface $entityManager): Response
    {
        /* @var SecurityUser $securityUser */
        $securityUser = $this->getUser();
        $loggedInTransmitter = $securityUser->getUser();

        if ((!$this->isGranted('ROLE_ADMINISTRATOR')) && (!$this->isGranted('ROLE_PREVIOUS_ADMIN'))) {
            $loginEvent = new UsersLogsEvents($loggedInTransmitter,
                new DateTimeImmutable('', new DateTimeZone('Europe/Paris')), 'Liste messages envoyés');
            $entityManager->persist($loginEvent);
            $entityManager->flush();
        }


        $transmittedMessagesList = $messagesRepository->findBy(['transmitter' => $loggedInTransmitter, 'isArchivedByTransmitter' => false], ['creationDate' => 'DESC']);

        return $this->render('message_sent/index.html.twig', [
            'TransmittedMessagesList' => $transmittedMessagesList,
        ]);
    }
}
