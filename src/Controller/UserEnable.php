<?php


namespace App\Controller;


use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use LogicException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserEnable extends AbstractController
{

    /**
     * @param User $user
     * @param UserRepository $userRepository
     * @param EntityManagerInterface $entityManager
     * @return Response
     * @Security("is_granted('ROLE_ADMINISTRATOR')")
     * @Route("/manager/user/userEnable/{userId}", name="user_enable", requirements={"userId" = "\d+"})
     * @Entity("user", expr="repository.find(userId)")
     */
    public function __invoke(User $user, UserRepository $userRepository, EntityManagerInterface $entityManager): Response
    {
        /**
         * @var User $userToBeEnabled
         */
        $userToBeEnabled = $userRepository->find($user->getId());

        if ($userToBeEnabled->getIsUserEnabled()){
            $userToBeEnabled->setIsUserNotEnabled();
        } else{
            $userToBeEnabled->setIsUserEnabled();
        }

        if ($userToBeEnabled->getRole() === 'ROLE_ADMINISTRATOR'){
            throw new LogicException('The user with ROLE_ADMINISTRATOR role can\'t be disabled');
        }

        $entityManager->flush();

        return $this->redirectToRoute('user_consulting', ['userId' => $user->getId()]);

    }
}
