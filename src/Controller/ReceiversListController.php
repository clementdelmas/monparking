<?php

namespace App\Controller;

use App\ControllerHelpers\Security\SecurityUser;
use App\Entity\UsersLogsEvents;
use App\Repository\UserRepository;
use DateTimeImmutable;
use DateTimeZone;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ReceiversListController extends AbstractController
{
    /**
     * @Route("/receivers/list", name="receivers_list")
     * @Security("is_granted('ROLE_ADMINISTRATOR') or is_granted('ROLE_RENTER') or is_granted('ROLE_TENANT')")
     * @param UserRepository $userRepository
     * @param EntityManagerInterface $entityManager
     * @return Response
     * @throws Exception
     */
    public function __invoke(UserRepository $userRepository, EntityManagerInterface $entityManager): Response
    {
        /* @var SecurityUser $securityUser */
        $securityUser = $this->getUser();
        $loggedInUser = $securityUser->getUser();

        if ((!$this->isGranted('ROLE_ADMINISTRATOR')) && (!$this->isGranted('ROLE_PREVIOUS_ADMIN'))) {
            $loginEvent = new UsersLogsEvents($loggedInUser,
                new DateTimeImmutable('', new DateTimeZone('Europe/Paris')), 'Choix liste destinataires');
            $entityManager->persist($loginEvent);
            $entityManager->flush();
        }

        $administrator = $userRepository->getUsersListPerRole('ROLE_ADMINISTRATOR', $loggedInUser);
        $renters = $userRepository->getUsersListPerRole('ROLE_RENTER', $loggedInUser);
        $tenants = $userRepository->getUsersListPerRole('ROLE_TENANT', $loggedInUser);

        return $this->render('receivers_list/index.html.twig', [
            'Administrator' => $administrator,
            'Renters' => $renters,
            'Tenants' => $tenants,
        ]);
    }
}
