<?php

namespace App\Controller;

use App\Entity\Day;
use App\Entity\ParkingSublease;
use App\Entity\User;
use App\Repository\DayRepository;
use App\Repository\ParkingSubleaseRepository;
use Doctrine\ORM\EntityManagerInterface;
use LogicException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class ParkingSubleaseRemoveController extends AbstractController
{
    /**
     * @Route("/parking/sublease/remove/{parkingSubleaseId}", name="parking_sublease_remove", requirements={"parkingSubleaseId" = "\d+"})
     * @Entity("parkingSublease", expr="repository.find(parkingSubleaseId)")
     * @Security("is_granted('ROLE_ADMINISTRATOR')")
     * @param ParkingSubleaseRepository $parkingSubleaseRepository
     * @param DayRepository $dayRepository
     * @param Request $request
     * @param ParkingSublease $parkingSublease
     * @param FormFactoryInterface $formFactory
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function __invoke(ParkingSubleaseRepository $parkingSubleaseRepository, DayRepository $dayRepository, Request $request,
                             ParkingSublease $parkingSublease, FormFactoryInterface $formFactory,
                             EntityManagerInterface $entityManager): Response
    {


        if ($parkingSublease->getIsTaken() === false || $parkingSublease->getIsSubleaseOpened() === true) {
            throw new LogicException('Not taken or isSubleaseOpened ParkingSublease can\'t be removed');
        }

//        $referer = $request->headers->get('referer', '/');


        $subleaseMonth = $parkingSublease->getDay()->getDate()->format('m');
        $subleaseYear = $parkingSublease->getDay()->getDate()->format('Y');

        /* @var Day $relatedDay */
        $relatedDay = $parkingSublease->getDay();
        $relatedDay->isFreeParkingSubleaseByDayIncreaseByOne();

        $parkingSublease->subleaseRetract();

        $entityManager->flush();

        return $this->redirectToRoute('calendar', ['month' => $subleaseMonth, 'year' => $subleaseYear]);

    }
}

