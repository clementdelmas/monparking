<?php

namespace App\Controller;

use App\ControllerHelpers\Security\SecurityUser;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserInsolvencyUnblockController extends AbstractController
{
    /**
     * @Route("/user/insolvency/unblock/{userId}", name="user_insolvency_unblock", requirements={"userId" = "\d+"})
     * @Entity("user", expr="repository.find(userId)")
     * @Security("is_granted('ROLE_ADMINISTRATOR')")
     * @param UserRepository $userRepository
     * @param EntityManagerInterface $entityManager
     * @param User $user
     * @return Response
     */
    public function __invoke(UserRepository $userRepository, EntityManagerInterface $entityManager, User $user): Response
    {
        if ($user->getInsolvencyImmunity() !== true) {
            $user->setInsolvencyImmunity(true);
            if ($user->getIsUserBlockedForInsolvency() === true) {
                $user->setIsUserBlockedForInsolvency(false);
            }
            $entityManager->flush();
        }elseif ($user->getInsolvencyImmunity() === true){
            $user->setInsolvencyImmunity(false);
            $entityManager->flush();
        }

        return $this->redirectToRoute('user_consulting', ['userId' => $user->getId()]);

    }
}
