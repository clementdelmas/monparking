<?php

namespace App\Controller;

use App\ControllerHelpers\CalendarData;
use App\DTO\Sublease\DTOParkingSubleaseTakerList;
use App\Entity\User;
use App\Entity\UsersLogsEvents;
use App\Repository\ParkingSubleaseRepository;
use App\Repository\UserRepository;
use DateTimeImmutable;
use DateTimeZone;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class RenterRentalsAllController extends AbstractController
{

    /**
     * @param ParkingSubleaseRepository $parkingSubleaseRepository
     * @param UserRepository $userRepository
     * @param string $category
     * @param EntityManagerInterface $entityManager
     * @return Response
     * @throws Exception
     * @Security("is_granted('ROLE_RENTER') or is_granted('ROLE_TENANT')")
     * @Route("/accounting/{category}/", name="accounting", requirements={"category" = "[a-z-A-Z]+"})
     */
    public function __invoke(ParkingSubleaseRepository $parkingSubleaseRepository, UserRepository $userRepository,
                             string $category, EntityManagerInterface $entityManager): Response
    {

        /* @var User $loggedInUser */
        $loggedInUser = $this->getUser()->getUser();
        $loggedUserId = $loggedInUser->getId();

        $openedPage = 'comptabilité - '.$category;

        if ((!$this->isGranted('ROLE_ADMINISTRATOR')) && (!$this->isGranted('ROLE_PREVIOUS_ADMIN'))) {
            $loginEvent = new UsersLogsEvents($loggedInUser,
                new DateTimeImmutable('', new DateTimeZone('Europe/Paris')), $openedPage);
            $entityManager->persist($loginEvent);
            $entityManager->flush();
        }


//        dd($loggedUser->getRole());

//        $initialUrl = $this->generateUrl('accounting', ['category' => $category], UrlGeneratorInterface::ABSOLUTE_PATH);
//        dd($url);

        $beginDate = new DateTimeImmutable('midnight', new DateTimeZone('Europe/Paris'));
        $beginDate = $beginDate->modify('first day of this month');
        $beginDate = $beginDate->modify('+1 month');
        $currentMonth = $beginDate->format('Y-m');
        $beginDate = $beginDate->modify('-24 month');

        $userSubleases = [];
        $contractors = [];

        if ($this->isGranted('ROLE_RENTER')) {
            $userSubleases = $parkingSubleaseRepository->findTabIsSubleaseTakenByRenter($loggedUserId, $beginDate);
            $contractors = $userRepository->getAllTenantUsersList();
        }elseif ($this->isGranted('ROLE_TENANT')) {
            $userSubleases = $parkingSubleaseRepository->findTabIsSubleaseTakenByTaker($loggedUserId, $beginDate);
            $contractors = $userRepository->getAllRenterUsersList();
        }

        $last24MonthsTab = [];

        /* @var DateTimeImmutable $beginDate */
        while ($beginDate->format('Y-m') <= $currentMonth){
            $last24MonthsTab[] = $beginDate->format('Y-m');
            $beginDate = $beginDate->modify('+1 month');
        }

        $last24MonthsTab = array_reverse($last24MonthsTab);

        $DTOParkingSublease = null;

        $perMonthAndContractorTab = [];
        foreach ($last24MonthsTab as $dateMonth) {
            foreach ($contractors as $contractor) {
                if ($this->isGranted('ROLE_RENTER')) {
                    $DTOParkingSublease = new DTOParkingSubleaseTakerList($dateMonth, $contractor, 'roleRenter', $userSubleases);
                }elseif($this->isGranted('ROLE_TENANT')) {
                    $DTOParkingSublease = new DTOParkingSubleaseTakerList($dateMonth, $contractor, 'roleTenant', $userSubleases);
                }

                if (!empty($DTOParkingSublease->getMonthSubleasesPerContractor())) {
                    if ($category === 'isPaid') {
                        if ($DTOParkingSublease->checkIfMonthIsPaid() || $DTOParkingSublease->getMonthSubleasesPerContractorTotal() === 0) {
                            $perMonthAndContractorTab[$dateMonth][$contractor->getId()][] = $DTOParkingSublease;
                        }
                    } elseif ($category === 'isNotPaid') {
                        if (!$DTOParkingSublease->checkIfMonthIsPaid() && $DTOParkingSublease->getMonthSubleasesPerContractorTotal() !== 0) {
                            $perMonthAndContractorTab[$dateMonth][$contractor->getId()][] = $DTOParkingSublease;
                        }
                    } else {
                        $perMonthAndContractorTab[$dateMonth][$contractor->getId()][] = $DTOParkingSublease;
                    }
                }
            }
        }

        $calendarData = new CalendarData([]);

        return $this->render('renter_rentals_list.html.twig', [
            'UserSubleases' => $userSubleases,
            'PerMonthAndContractorTab' => $perMonthAndContractorTab,
            'Category' => $category,
            'UserRole' => $loggedInUser->getRole(),
            'CalendarData' => $calendarData,
            'LoggedUser' => $loggedInUser,
        ]);
    }
}
