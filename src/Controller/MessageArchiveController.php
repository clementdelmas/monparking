<?php

namespace App\Controller;

use App\Entity\Messages;
use Doctrine\ORM\EntityManagerInterface;
use LogicException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MessageArchiveController extends AbstractController
{
    /**
     * @Route("/message/archive/{messageId}", name="message_archive", requirements={"\d+"})
     * @Entity("message", expr="repository.find(messageId)")
     * @param Messages $message
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    public function __invoke(Messages $message, EntityManagerInterface $entityManager): Response
    {
        $loggedInUser = $this->getUser()->getUser();

        if ($this->isGranted('ROLE_PREVIOUS_ADMIN') && ($message->getTransmitter()->getUsername() !== 'gr')) {
            throw new LogicException('Unauthorized ROLE_PREVIOUS_ADMIN user !');
        }

        if ($message->getReadingDate() === null){
            throw new LogicException('Only read messages can be deleted');
        }


        if ($loggedInUser === $message->getReceiver() || ($this->isGranted('ROLE_ADMINISTRATOR')
                && $message->getReceiver()->getUsername() === 'gr')) {
            $message->setIsArchivedByReceiver(true);
            $entityManager->flush();
        }elseif ($loggedInUser === $message->getTransmitter() || ($this->isGranted('ROLE_ADMINISTRATOR')
                && $message->getReceiver()->getUsername() === 'gr')) {
            $message->setIsArchivedByTransmitter(true);
            $entityManager->flush();
        }else{
            new LogicException('Unauthorized logged in user !');
        }

        if ($loggedInUser === $message->getReceiver()) {
            return $this->redirectToRoute('message_receive', ['inboxType' => 'inbox']);
        }

        return $this->redirectToRoute('message_sent');
    }
}
