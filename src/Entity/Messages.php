<?php

namespace App\Entity;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MessagesRepository")
 */
class Messages
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime_immutable")
     */
    private $creationDate;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private $readingDate;


    /**
     * @ORM\Column(type="string", length=2000)
     */
    private $message;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="sentMessages")
     * @ORM\JoinColumn(nullable=false)
     */
    private $transmitter;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="receivedMessages")
     * @ORM\JoinColumn(nullable=false)
     */
    private $receiver;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $receiptAcknowledgement;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isArchivedByReceiver;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isArchivedByTransmitter;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreationDate(): ?DateTimeImmutable
    {
        return $this->creationDate;
    }

    public function setCreationDate(DateTimeImmutable $creationDate): self
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    public function getReadingDate(): ?DateTimeImmutable
    {
        return $this->readingDate;
    }

    public function setReadingDate(DateTimeImmutable $readingDate): self
    {
        $this->readingDate = $readingDate;

        return $this;
    }


    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function __construct(User $transmitter, string $message, User $receiver)
    {
        $this->transmitter = $transmitter;
        $this->creationDate = new DateTimeImmutable();
        $this->message = $message;
        $this->receiver = $receiver;
        $this->receiptAcknowledgement = false;
        $this->isArchivedByReceiver = false;
        $this->isArchivedByTransmitter = false;
    }

    public function getTransmitter(): ?User
    {
        return $this->transmitter;
    }

    public function setTransmitter(?User $transmitter): self
    {
        $this->transmitter = $transmitter;

        return $this;
    }

    public function getReceiver(): ?User
    {
        return $this->receiver;
    }

    public function setReceiver(?User $receiver): self
    {
        $this->receiver = $receiver;

        return $this;
    }

    public function getReceiptAcknowledgement(): ?bool
    {
        return $this->receiptAcknowledgement;
    }

    public function setReceiptAcknowledgement(?bool $receiptAcknowledgement): self
    {
        $this->receiptAcknowledgement = $receiptAcknowledgement;

        return $this;
    }

    public function getIsArchivedByReceiver(): ?bool
    {
        return $this->isArchivedByReceiver;
    }

    public function setIsArchivedByReceiver(?bool $isArchivedByReceiver): self
    {
        $this->isArchivedByReceiver = $isArchivedByReceiver;

        return $this;
    }

    public function getIsArchivedByTransmitter(): ?bool
    {
        return $this->isArchivedByTransmitter;
    }

    public function setIsArchivedByTransmitter(?bool $isArchivedByTransmitter): self
    {
        $this->isArchivedByTransmitter = $isArchivedByTransmitter;

        return $this;
    }
}
