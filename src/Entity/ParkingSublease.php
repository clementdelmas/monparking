<?php


namespace App\Entity;


use DateTimeImmutable;
use Doctrine\ORM\Mapping\OrderBy;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Exception;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ParkingSubleaseRepository")
 * @ORM\Table(name="parkingsublease", uniqueConstraints={@UniqueConstraint(name="idx_day_user", columns={"day_id", "user_id"})})
 */
class ParkingSublease
{

    const PRICE = 5.0;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }


    /**
     * @ORM\Column(type="datetime_immutable")
     * @OrderBy({"name" = "ASC"})
     */
    private $creationDate;


    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private $paidAt;


    /**
     * @return DateTimeImmutable|null
     */
    public function getDate(): ?DateTimeImmutable
    {
        return $this->creationDate;
    }


    /**
     * @ORM\ManyToOne(targetEntity="Day", inversedBy="parkingSublease", cascade={"persist"})
     */
    private $day;


    /**
     * @return Day
     */
    public function getDay(): Day
    {
        return $this->day;
    }


    /**
     * @ORM\Column(type="integer")
     */
    private $subleaseParkingNumber;



    /**
     * @return int
     */
    public function getSubleaseParkingNumber(): int
    {
        return $this->subleaseParkingNumber;
    }



    /**
     * @return int|null
     */
    public function getRenterParkingNumber(): ?int
    {
        return $this->user->getParkingNumber();
    }


    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }


    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }


    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="parkingSublease", cascade={"persist"})
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="parkingSubleaseAsTaker", cascade={"persist"})
     */
    private $taker;


    /**
     * @return User|null
     */
    public function getTaker(): ?User
    {
        return $this->taker;
    }


    /**
     * @param User $taker
     */
    public function setTaker(User $taker): void
    {
        $this->taker = $taker;
    }


    /**
     * @ORM\Column(type="float")
     */
    private $price;


    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }


    /**
     * @param float $price
     */
    private function setPrice(float $price): void
    {
        $this->price = $price;
    }


    /**
     * @ORM\Column(type="boolean")
     */
    private $isSubleaseOpened;


    public function subleaseRetract(): void
    {
        $this->isSubleaseOpened = true;
        $this->isTaken = false;
        $this->taker = null;
        $this->paidAt = null;
        $this->dueDate = null;
    }


    /**
     * @return bool
     */
    public function getIsSubleaseOpened(): bool
    {
        return $this->isSubleaseOpened;
    }


    public function sublease(): void
    {
        $this->isSubleaseOpened = true;
        $this->price = self::PRICE;
    }


    public function unSublease(): void
    {
        $this->isSubleaseOpened = false;
    }


    /**
     * @ORM\Column(type="boolean")
     */
    private $isTaken;

    /**
     * @ORM\Column(type="datetime_immutable", nullable=true)
     */
    private $dueDate;


    /**
     * @return bool
     */
    public function getIsTaken(): bool
    {
        return $this->isTaken;
    }



    public function setIsTaken(): void
    {
        $this->isTaken = true;
        $this->unSublease();
    }


    public function markAsPaid(): void
    {
        $this->paidAt = new DateTimeImmutable();
        if ($this->dueDate !== null) {
            $this->dueDate = null;
        }
    }


    public function markAsNotPaid(): void
    {
        $this->paidAt = null;
        $this->setDueDate();
    }


    public function getIsPaid(): bool
    {
        return $this->paidAt !== null;
    }


    public function getIsNotPaid(): bool
    {
        return $this->paidAt === null;
    }


    public function getPaidAt(): DateTimeImmutable
    {
        return $this->paidAt;
    }



    /**
     * ParkingSublease constructor.
     * @param Day $day
     * @param User $user
     * @throws Exception
     */
    public function __construct(Day $day, User $user)
    {
        $this->day = $day;
        $this->isSubleaseOpened = true;
        $this->isTaken = false;
        $this->creationDate = new DateTimeImmutable();
        $this->user = $user;
        $this->subleaseParkingNumber = $user->getParkingNumber();
        $this->price = self::PRICE;
        $this->paidAt = null;
    }

    public function getDueDate(): ?DateTimeImmutable
    {
        return $this->dueDate;
    }

    public function setDueDate(): self
    {
        $dayDueDate = $this->day->getDate();
        $dayDueDate = $dayDueDate->modify('last day of this month');
        $dayDueDate = $dayDueDate->modify('+ 10 days');

        $this->dueDate = $dayDueDate;

        return $this;
    }
}
