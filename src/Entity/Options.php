<?php


namespace App\Entity;

use Doctrine\ORM\Mapping\OrderBy;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Options
 * @ORM\Entity(repositoryClass="App\Repository\OptionsRepository")
 */
class Options
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }


    /**
     * @ORM\Column(type="float")
     */
    private $price;


    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }


    /**
     * @ORM\Column(type="float")
     */
    private $partialPrice;


    /**
     * @return float
     */
    public function getPartialPrice(): float
    {
        return $this->partialPrice;
    }



}